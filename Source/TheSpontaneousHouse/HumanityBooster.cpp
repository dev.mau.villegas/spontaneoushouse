// Fill out your copyright notice in the Description page of Project Settings.


#include "HumanityBooster.h"


// Sets default values
AHumanityBooster::AHumanityBooster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	MeshComp->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(TEXT("StaticMesh'/Game/Geometry/PieceOfArt.PieceOfArt'"));
	if (NewMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(NewMesh.Object);
		MeshComp->SetRelativeLocation(FVector(-20.0f, 0.0f, 0.0f));
		MeshComp->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
		MeshComp->SetWorldScale3D(FVector(1.0f));
		MeshComp->SetCollisionProfileName(TEXT("OverlapAll"));
	}

}

// Called when the game starts or when spawned
void AHumanityBooster::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHumanityBooster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float AHumanityBooster::UseBooster(float HumanityAmount, float DeltaTime)
{
	if (bVisited == false)
	{
		bVisited = true;
		AmountAvailable = (100 - HumanityAmount) / 2;
	}

	if (AmountAvailable > 0)
	{
		AmountAvailable = AmountAvailable - (DeltaTime * Multiplier);
		return (DeltaTime * Multiplier * (-1));
	}

	return 0.0f;
}