// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Spontaneous_HUD.generated.h"

/**
 * 
 */
UCLASS()
class THESPONTANEOUSHOUSE_API ASpontaneous_HUD : public AHUD
{
	GENERATED_BODY()

	public:
	ASpontaneous_HUD();

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	TSubclassOf<UUserWidget> HUD_Class;

	UUserWidget* HUD_Widget = nullptr;

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
