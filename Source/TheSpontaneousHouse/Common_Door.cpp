// Fill out your copyright notice in the Description page of Project Settings.

#include "Common_Door.h"

#include "Components/TextBlock.h"
#include "UObject/ConstructorHelpers.h"


ACommon_Door::ACommon_Door()
{
	PrimaryActorTick.bCanEverTick = true;

	//Rep Fisica
	Box_Trigger_Component = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision"));
	RootComponent = Box_Trigger_Component;
	Box_Trigger_Component->SetBoxExtent(FVector(100.0f, 50.0f, 100.0f));
	Box_Trigger_Component->SetCollisionProfileName(TEXT("OverlapAll"));
	Box_Trigger_Component->SetGenerateOverlapEvents(true);
	Box_Trigger_Component->bMultiBodyOverlap = true;
	
	//Rep Visual
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	MeshComp->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> NewMesh(TEXT("StaticMesh'/Game/StarterContent/Props/SM_Door.SM_Door'"));
	if (NewMesh.Succeeded())
	{
		MeshComp->SetStaticMesh(NewMesh.Object);
		MeshComp->SetRelativeLocation(FVector(0.0f, 45.0f, -110.0f));
		MeshComp->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
		MeshComp->SetWorldScale3D(FVector(1.0f));
		MeshComp->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	}
	
	Box_Trigger_Component->OnComponentBeginOverlap.AddDynamic(this, &ACommon_Door::OpenDoor);
	Box_Trigger_Component->OnComponentEndOverlap.AddDynamic(this, &ACommon_Door::CloseDoor);
	
	CurrentYaw = MeshComp->GetComponentRotation().Yaw;
	OriginalYaw = CurrentYaw;

	TargetYaw += CurrentYaw;

	DoorSound = CreateDefaultSubobject<UAudioComponent>(TEXT("OpenDoorSound"));
	DoorSound->SetupAttachment(RootComponent);
	
	static ConstructorHelpers::FObjectFinder<USoundBase> NewOpenSound(TEXT("SoundWave'/Game/Sounds/Open_Door_1.Open_Door_1'"));
	if (NewOpenSound.Succeeded())
	{
		OpenDoorSound = NewOpenSound.Object;
	}
	
	static ConstructorHelpers::FObjectFinder<USoundBase> NewCloseSound(TEXT("SoundWave'/Game/Sounds/Close_Door_1.Close_Door_1'"));
	if (NewCloseSound.Succeeded())
	{
		CloseDoorSound = NewCloseSound.Object;
	}
	
	static ConstructorHelpers::FClassFinder<UOpenDoor> NewDoorSignWidget(TEXT("/Game/Widgets/OpenDoor_WBP"));
	if (NewDoorSignWidget.Succeeded() && NewDoorSignWidget.Class != nullptr)
	{
		DoorSignWidget = NewDoorSignWidget.Class;
	}

}

void ACommon_Door::DisplayOpenSign()
{
	if(OpenSign != nullptr)
	{
		OpenSign->AddToViewport();
		OpenSign->DisplayText->SetText(FText::FromString("Press E to open"));
		OpenSign->OpenCost->SetText(FText::FromString(FString::Printf(TEXT("%d humanity required"), OpeningCost)));
	}
}

void ACommon_Door::HideOpenSign()
{
	if(OpenSign != nullptr)
	{
		OpenSign->RemoveFromViewport();
	}
}

// Called when the game starts or when spawned
void ACommon_Door::BeginPlay()
{
	Super::BeginPlay();
	OpenSign = CreateWidget<UOpenDoor>(GetWorld(), DoorSignWidget);
}

// Called every frame
void ACommon_Door::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bCanBeOpened)
	{
		FRotator DoorRotation = MeshComp->GetRelativeRotation();
		CurrentYaw = FMath::FInterpTo(CurrentYaw, TargetYaw, DeltaTime, OpenSpeed);
		DoorRotation.Yaw = CurrentYaw;
		MeshComp->SetRelativeRotation(DoorRotation);
	} else
	{
		FRotator DoorRotation = MeshComp->GetRelativeRotation();
		CurrentYaw = FMath::FInterpTo(CurrentYaw, OriginalYaw, DeltaTime, OpenSpeed);
		DoorRotation.Yaw = CurrentYaw;
		MeshComp->SetRelativeRotation(DoorRotation);
		// Set sound when door closes 
	}
}

void ACommon_Door::OpenDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bUserInputActivated = true;
	EnableInput(GetWorld()->GetFirstPlayerController());
	PlayerInteracting = Cast<ABerserker>(OtherActor);
	DisplayOpenSign();
}

void ACommon_Door::CloseDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bCanBeOpened = false;
	PlayerInteracting = nullptr;
	bUserInputActivated = false;
	DisableInput(GetWorld()->GetFirstPlayerController());
	MeshComp->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	HideOpenSign();
}

void ACommon_Door::ManageOpenDoor()
{
	if (bUserInputActivated)
	{
		bCanBeOpened = true;
		bUserInputActivated = false;
		HideOpenSign();
		DisableInput(GetWorld()->GetFirstPlayerController());
		if (PlayerInteracting != nullptr)
		{
			PlayerInteracting->SpendHumanity(WasOpened ? OpeningCost/2 : OpeningCost);
			MeshComp->SetCollisionProfileName(TEXT("OverlapAll"));
			WasOpened = true;
		}
		if(DoorSound != nullptr)
		{
			DoorSound->SetSound(OpenDoorSound);
			DoorSound->Play();
		}
	}
}
