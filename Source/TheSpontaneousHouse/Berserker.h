// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/AudioComponent.h"
#include "BerserkerPlayerController.h"
#include "HumanityBooster.h"
#include "Berserker.generated.h"

UCLASS()
class THESPONTANEOUSHOUSE_API ABerserker : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABerserker();

	ABerserkerPlayerController* PC;

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void MoveForward(float AxisValue);
	void MoveHorizontal(float AxisValue);
	void Turn(float AxisValue);
	void TurnY(float AxisValue);

	void SpendHumanity(float HumanityAmount);

	UFUNCTION(BlueprintCallable)
	bool GetHasKey() const;
	
	void SetHasKey();

	AHumanityBooster* HumanityBooster = nullptr;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnyWhere, Category = Camera)
	USpringArmComponent* SpringArm;

	UPROPERTY(EditAnyWhere, Category = Camera)
	UCameraComponent* Camera;

	UPROPERTY(EditAnyWhere, Category = SKMesh)
	USkeletalMeshComponent* Skeleton;

	void CheckTrace(float DeltaTime);
};
