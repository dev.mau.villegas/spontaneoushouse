// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BerserkerPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class THESPONTANEOUSHOUSE_API ABerserkerPlayerController : public APlayerController
{
	GENERATED_BODY()

	float HumanityLevel = 100.0f;
	
	bool bHasKey = false;
	float DamageTimerDelay = 10.0f;
	float DamageTimer = 0.0f;

	USoundBase* PickUpKeySound = nullptr;
	
	bool bIsPlayerAlive = true;
	ABerserkerPlayerController();
	
public:
	
	void SetupInputComponent();

	void InputMoveForward(float AxisValue);
	void InputMoveHorizontal(float AxisValue);
	void InputTurn(float AxisValue);
	void InputTurnY(float AxisValue);
	void InputJump();
	void InputStopJump();
	void InputRestartGame();

	bool bCanRestart = false;

	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable)
	float GetHumanityLevel() const { return HumanityLevel; }

	UFUNCTION(BlueprintCallable)
	void SpendHumanity(float HumanityAmount) { HumanityLevel -= (HumanityAmount * HumanityLevel) / 100; }

	UFUNCTION(BlueprintCallable)
	void SpontaneousDamage(float HumanityAmount);
	
	UFUNCTION(BlueprintCallable)
	void SetHasTheKey();

	UFUNCTION(BlueprintCallable)
	bool GetHasKey() const { return bHasKey; }
	
	UFUNCTION(BlueprintNativeEvent, Category = "Die")
    void DIE();

	UFUNCTION(BlueprintNativeEvent, Category = "Die")
	void WIN();

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	TSubclassOf<UUserWidget> WinningWidget;

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	TSubclassOf<UUserWidget> LosingWidget;

	UPROPERTY()
	UUserWidget* LevelEndingWidget = nullptr;

	UPROPERTY(EditAnyWhere, Category = "Sounds")
	UAudioComponent* KeySound = nullptr;
};
