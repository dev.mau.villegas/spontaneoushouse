#include "BerserkerPlayerController.h"
#include "Berserker.h"
#include "Spontaneous_HUD.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

ABerserkerPlayerController::ABerserkerPlayerController()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> NewLosingWidget(TEXT("/Game/Widgets/Lose_WBP"));
	if (NewLosingWidget.Succeeded() && NewLosingWidget.Class != nullptr)
	{
		LosingWidget = NewLosingWidget.Class;
	}

	static ConstructorHelpers::FClassFinder<UUserWidget> NewWinningWidget(TEXT("/Game/Widgets/Win_WBP"));
	if (NewWinningWidget.Succeeded() && NewWinningWidget.Class != nullptr)
	{
		WinningWidget = NewWinningWidget.Class;
	}

	KeySound = CreateDefaultSubobject<UAudioComponent>(TEXT("PickupKeySound"));
	KeySound->SetupAttachment(RootComponent);
	
	static ConstructorHelpers::FObjectFinder<USoundBase> NewOpenSound(TEXT("SoundWave'/Game/Sounds/PickupKey.PickupKey'"));
	if (NewOpenSound.Succeeded())
	{
		PickUpKeySound = NewOpenSound.Object;
		KeySound->SetSound(PickUpKeySound);
		KeySound->bAutoActivate = false;
	}
}

void ABerserkerPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAxis("MoveForward", this, &ABerserkerPlayerController::InputMoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABerserkerPlayerController::InputMoveHorizontal);
	InputComponent->BindAxis("TurnRate", this, &ABerserkerPlayerController::InputTurn);
	InputComponent->BindAxis("TurnRateY", this, &ABerserkerPlayerController::InputTurnY);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ABerserkerPlayerController::InputJump);
	InputComponent->BindAction("Jump", IE_Released, this, &ABerserkerPlayerController::InputStopJump);
	InputComponent->BindAction("Restart", IE_Pressed, this, &ABerserkerPlayerController::InputRestartGame);

}

void ABerserkerPlayerController::InputMoveForward(float AxisValue)
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->MoveForward(AxisValue);
	}
}

void ABerserkerPlayerController::InputMoveHorizontal(float AxisValue)
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->MoveHorizontal(AxisValue);
	}
}

void ABerserkerPlayerController::InputTurn(float AxisValue)
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->Turn(AxisValue);
	}
}

void ABerserkerPlayerController::InputTurnY(float AxisValue)
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->TurnY(AxisValue);
	}
}

void ABerserkerPlayerController::InputJump()
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->Jump();
	}
}

void ABerserkerPlayerController::InputStopJump()
{
	ABerserker* pawn = Cast<ABerserker>(GetPawn());
	if (pawn != nullptr)
	{
		pawn->StopJumping();
	}
}

void ABerserkerPlayerController::InputRestartGame()
{
	if (bCanRestart)
	{
		RestartLevel();
	}
}

void ABerserkerPlayerController::Tick(float DeltaTime)
{
	if (bIsPlayerAlive)
	{
		if (GetWorld()->GetTimeSeconds() - DamageTimer > DamageTimerDelay)
		{
			SpontaneousDamage(1.0f);
			DamageTimer = GetWorld()->GetTimeSeconds();
		}
	}
}

void ABerserkerPlayerController::SpontaneousDamage(float HumanityAmount)
{
	HumanityLevel -= HumanityAmount;
	if (HumanityLevel <= 0)
	{
		DIE();
		bIsPlayerAlive = false;
	}
}

void ABerserkerPlayerController::SetHasTheKey()
{
	bHasKey = true;
	KeySound->Play();
}

void ABerserkerPlayerController::DIE_Implementation()
{
	//UnPossess();
	PlayerCameraManager->StartCameraFade(0.0f, 1.0f, 2.0f, FLinearColor::Black, true, true);
	Cast<ASpontaneous_HUD>(GetHUD())->HUD_Widget->RemoveFromParent();

	LevelEndingWidget = CreateWidget(GetWorld(), LosingWidget);
	LevelEndingWidget->AddToViewport();
	bCanRestart = true;
}

void ABerserkerPlayerController::WIN_Implementation()
{
	//UnPossess();
	PlayerCameraManager->StartCameraFade(0.0f, 1.0f, 3.0f, FLinearColor::White, true, true);
	Cast<ASpontaneous_HUD>(GetHUD())->HUD_Widget->RemoveFromParent();
	
	LevelEndingWidget = CreateWidget(GetWorld(), WinningWidget);
	LevelEndingWidget->AddToViewport();

	bCanRestart = true;
}
