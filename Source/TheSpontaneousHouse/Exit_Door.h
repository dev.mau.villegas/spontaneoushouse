// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Berserker.h"
#include "OpenDoor.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Exit_Door.generated.h"

UCLASS()
class THESPONTANEOUSHOUSE_API AExit_Door : public AActor
{
	GENERATED_BODY()
	
	bool bUserInputActivated = false;

	ABerserker* PlayerInteracting = nullptr;
	
public:	
	// Sets default values for this actor's properties
	AExit_Door();

	UFUNCTION(BlueprintCallable)
	void ManageOpenDoor();
	
	UPROPERTY(EditAnyWhere, Category = CollisionComp)
	UBoxComponent* Box_Trigger_Component = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Assets")
	UStaticMeshComponent* MeshComp;

	UFUNCTION()
	void OpenDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void CloseDoor(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere, Category="Spontaneous Behavior")
	TSubclassOf<UOpenDoor> DoorSignWidget;

	UPROPERTY()
	UOpenDoor* OpenSign = nullptr;
	
	UFUNCTION(BlueprintCallable)
	void DisplayOpenSign();

	UFUNCTION(BlueprintCallable)
	void HideOpenSign();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
