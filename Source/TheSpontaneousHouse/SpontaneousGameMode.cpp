// Fill out your copyright notice in the Description page of Project Settings.

#include "SpontaneousGameMode.h"
#include "Berserker.h"
#include "BerserkerPlayerController.h"
#include "Spontaneous_HUD.h"

ASpontaneousGameMode::ASpontaneousGameMode()
{
	PlayerControllerClass = ABerserkerPlayerController::StaticClass();

	auto PlayerBP = ConstructorHelpers::FClassFinder<ABerserker>(
		TEXT("Class'/Script/TheSpontaneousHouse.Berserker'")
	);
	if (PlayerBP.Succeeded())
	{
		DefaultPawnClass = PlayerBP.Class;
	}
	
	auto HUD_BP = ConstructorHelpers::FClassFinder<ASpontaneous_HUD>(
		TEXT("Class'/Script/TheSpontaneousHouse.Spontaneous_HUD'")
	);
	if (HUD_BP.Succeeded())
	{
		HUDClass = HUD_BP.Class;
	}
}
