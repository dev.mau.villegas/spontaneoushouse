// Fill out your copyright notice in the Description page of Project Settings.


#include "Spontaneous_HUD.h"
#include "Blueprint/UserWidget.h"

ASpontaneous_HUD::ASpontaneous_HUD()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> NewHUD(TEXT("/Game/Widgets/HUD_WBP"));
	if (NewHUD.Succeeded() && NewHUD.Class != nullptr)
	{
		HUD_Class = NewHUD.Class;
	}

	HUD_Widget = CreateWidget<UUserWidget>(GetWorld(), HUD_Class);
}

void ASpontaneous_HUD::BeginPlay()
{
	if (HUD_Widget != nullptr)
	{
		HUD_Widget->AddToViewport();
	}
}


