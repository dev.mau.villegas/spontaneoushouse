// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/BoxComponent.h"

#include "Door_Opener.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESPONTANEOUSHOUSE_API UDoor_Opener : public UActorComponent
{
	GENERATED_BODY()

	float TotalMassOfActors() const;

	UPROPERTY(EditAnywhere)
	float TotalMass = 70.0f;

	UPROPERTY(EditAnywhere)
	float TargetYaw = 90.0f;

	UPROPERTY(EditAnywhere)
	float DoorCloseDelay = 2.0f;

	UPROPERTY(EditAnywhere)
	float OpenSpeed = 0.5f;

	UPROPERTY(EditAnywhere)
	float CloseSpeed = 0.8f;

	UPROPERTY(EditAnywhere)
	UBoxComponent* Overlap_Box = nullptr;

	UPROPERTY(EditAnywhere)
	UAudioComponent* AudioComponent = nullptr;

	float DoorLastOpened = 0.0f;
	float OriginalYaw;
	float CurrentYaw;
	bool bOpenDoor = true;
	
public:	
	UDoor_Opener();

protected:
	virtual void BeginPlay() override;
	
	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	void FindComponents();

public:	
	virtual void TickComponent(float DeltaTime,
								ELevelTick TickType,
								FActorComponentTickFunction* ThisTickFunction
								) override;
		
};
